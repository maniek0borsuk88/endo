<?php
// sprawdzanie aktywnej sesji by nie powtarzać
$session = session_status();
if($session == PHP_SESSION_NONE) {
	session_start();
}

 /**
 *  Generowanie tokenu zabezpieczającego
 */
if(!isset($_SESSION['token'])){

	$_SESSION['token'] = sha1(uniqid((string)mt_rand(), TRUE));

}


// Define
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', __DIR__ . DS);
define('CONTROLLER_PATH', ROOT . 'inc' . DS . 'controller' . DS);
define('PUBLIC_PATH', ROOT . 'public' . DS);
define('SERVER_PATH_LINK', $_SERVER['REQUEST_URI']);


define('DB_NAME', 'endo');
define('DB_PASS', 'mariusz888');
define('DB_HOST', 'localhost');
define('DB_USER', 'mariusz888');


if(file_exists( ROOT . 'vendor/autoload.php' )) {

	require_once ROOT . 'vendor/autoload.php';

}


require_once 'public/public.php';


$array = new \endo\arrays\ErrorCode();