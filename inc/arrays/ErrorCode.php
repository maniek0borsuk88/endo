<?php
/**
 * Array Code Error
 */

namespace endo\arrays;


class ErrorCode {


	public static function registerErrorCode(string $param) {

		$register = [

			'name' => '*Proszę wpisać poprawnie imię.',
			'surname' => '*Proszę wpisać poprawne nazwisko.',
			'email' => '*Proszę wpisać poprawny adres email.',
			'password' => '*Prosze podać poprawne hasło.',
			'password_repeat' => '*Proszę powtórzyć hasło.',
			'password2' => '*Podane  hasła sa nie zgodne.',
			'country' => '*Proszę podać poprawny kraj.',
			'date' => '*Proszę podac poprawny format daty.',
			'verifyPass' => '*Podane hasła są niezgodne.',
			'verifyTitle' => 'Prosze wpisać tytuł.',
			'verifyPrice' => 'Prosze wpisać poprawną cenę.',
			'verifyLitres' => 'Prosze wpisać poprawna ilość litrów.'
		];

		return $register[$param];

	}

}