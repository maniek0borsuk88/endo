<?php

namespace endo\db;


class DB_Connect {

	protected $db;

	public function __construct($db=NULL) {

		$dsn = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";encoding=utf8";

		try {

			$this->db = new \PDO($dsn, DB_USER,DB_PASS);
			$this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			$this->db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);

			} catch(\Exception $e) {

				die($e->getMessage());

			}

	}


	/**
	 *
	 * Downloads Select Db records
	 *
	 */
	public function queryDb($query,$key) {


		$result = $this->db->query($query);
		$result->execute();
		$vals = $result->fetchAll();

		foreach ( $vals as $val ) {

			return $val[$key];

		}

	}



	/**
	 *
	 * Downloads Select Db records
	 *
	 */
	public function queryAllDb($query) {


		$result = $this->db->query($query);
		$result->execute();
		$vals = $result->fetchAll();

		return $vals;

	}

}