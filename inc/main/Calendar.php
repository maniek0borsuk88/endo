<?php
/**
 * Class Calendar
 */

namespace endo\main;




use endo\db\DB_Connect;

class Calendar extends DB_Connect {

	public $next;
	public $prev;
	public $weeks;
	public $week;
	public $html_title;
	public $month;


		public function __construct() {

			$this->paginationCalendar();

		}



	public function paginationCalendar() {

		date_default_timezone_set('Europe/Warsaw');

		if(isset($_GET['date'])) {

			$ym = $_GET['date'];

		} else {

			$ym = date('Y-m');

		}

		$timestamp = strtotime($ym, "-01");
		if($timestamp === false) {

			$timestamp = time();

		}

		// Today
		$today = date('Y-m-d', time());


		$miesiac = array('', 'Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Pażdziernik', 'Listopad', 'Grudzień');

		$this->html_title = $miesiac[date('n', $timestamp)] . ' ' . date(' Y ', $timestamp);

		// Title
		//$this->html_title = date('F Y', $timestamp);
		$this->month = date('Y-m',$timestamp);
		$_SESSION['month'] = $this->month;

		// Next and Prev
		$this->prev = date('Y-m',mktime(0,0,0, date('m', $timestamp)-1,1,date('Y',$timestamp)));
		$this->next = date('Y-m',mktime(0,0,0, date('m', $timestamp)+1,1,date('Y',$timestamp)));


		$day_count = date('t', $timestamp);

		$str = date('w', mktime(0,0,0, date('m', $timestamp),0 , date('Y',$timestamp)));

		$this->weeks = [];
		$this->week = '';

		$this->week .= str_repeat('<td></td>', $str);

		for ($day = 1; $day <= $day_count;$day++ , $str++) {

			$date = $ym . '-' . $day;

			if($today == $date) {

				$this->week .= '<td class="today"><a href="calendar.php?date=' . $ym . '">' . $day ;

			} else {

				$this->week .= '<td><a href="calendar.php?date=' . $ym . '">' . $day ;

			}

			$this->week .= '</a></td>';


			if($str % 7 == 6 || $day == $day_count) {

				if($day == $day_count) {

					$this->week .= str_repeat('<td class="' . date('FY') . $day . '"></td>', 6 - ($str % 7));

				}

				$this->weeks[] = '<tr>' . $this->week . '</tr>';

				$this->week = '';

			}

		}

	}





}
