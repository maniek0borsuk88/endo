<?php
/**
 * Created by PhpStorm.
 * User: Maniek
 * Date: 2018-08-16
 * Time: 20:58
 */

namespace endo\main;


use endo\db\DB_Connect;

class Delete extends DB_Connect {

	public $date;
	public $date_end;

	public function delete($query) {

		if(isset($query)) {

			$result = $this->db->prepare($query);
			$result->execute();

		}

	}

	public function sumTank($query, $index) {

		$_SESSION['month_sum'] = $_SESSION['month'] . '-01';
		$_SESSION['month_sum_end'] = $_SESSION['month'] . '-31';

		$result = $this->db->prepare($query);
		$result->execute();
		$row = $result->fetchAll();

		foreach ($row as $rov) {

			echo $rov[$index];

		}

	}

}