<?php

namespace endo\main;

use endo\arrays\ErrorCode;

class FormRegister extends Validation {

	public function formRegister() {

		if(isset($_POST['register'])) {

			if(!empty($_POST['name']) && !empty($_POST['surname']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['repeat_password']) && !empty($_POST['country']) && !empty($_POST['day_birth'])) {

				// Validation Name register
				$this->name = $this->stringTextValidation($_POST['name'], ErrorCode::registerErrorCode('name'));
				$name = htmlentities($this->name, ENT_QUOTES);

				// Validation Surname register
				$this->surname = $this->stringTextValidation($_POST['surname'], ErrorCode::registerErrorCode('surname'));
				$surname = htmlentities($this->surname, ENT_QUOTES);

				// Validation Email register
				$this->email = $this->emailValidation($_POST['email'], ErrorCode::registerErrorCode('email'));
				$email = htmlentities($this->email, ENT_QUOTES);
				$email = $this->emailVerifyDb('users',$email, 'email');


				// Validation Password register
				$this->password = $this->validPassword($_POST['password'], ErrorCode::registerErrorCode('password'));
				$password = htmlentities($this->password, ENT_QUOTES) ;

				// Validation Password Repeat register
				$this->repeat_password = $this->validPassword($_POST['repeat_password'], ErrorCode::registerErrorCode('password_repeat'));
				$repeat_password = htmlentities($this->repeat_password, ENT_QUOTES);

				// Validation Country register
				$this->country = $this->stringTextValidation($_POST['country'],ErrorCode::registerErrorCode('country'));
				$country = htmlentities($this->country, ENT_QUOTES);

				// Validation Day Birth register
				$this->day_birth = $this->validDate($_POST['day_birth'], ErrorCode::registerErrorCode('date'));
				$day_birth = htmlentities($this->day_birth, ENT_QUOTES);

				$passwordVer = $this->verificationPasswords($password,$repeat_password, ErrorCode::registerErrorCode('verifyPass'));

				$date_register = date('Y-m-d G:i:s');

				$img_register  = htmlentities( 'img/nouser.png');

				if($passwordVer === TRUE && $email != FALSE) {

					$password = password_hash($password, PASSWORD_BCRYPT);

					$query = "INSERT INTO users(imie,nazwisko,email,pass,pass_repeat,kraj,date_birth,date_register,img_profil) VALUES(:imie, :nazwisko,:email,:pass,:pass_repeat,:kraj,:date_birth,:dateregister,:imgprofil)";
					$result = $this->db->prepare($query);
					$result->bindParam(":imie", $name);
					$result->bindParam(":nazwisko", $surname);
					$result->bindParam(":email", $email);
					$result->bindParam(":pass", $password);
					$result->bindParam(":pass_repeat", $repeat_password);
					$result->bindParam(":kraj", $country);
					$result->bindParam(":date_birth", $day_birth);
					$result->bindParam(":dateregister", $date_register);
					$result->bindParam(":imgprofil", $img_register);
					$result->execute();

					echo 'Dziękujemy za zarejestrowanie się w serwisie Endo';

				} else {

					echo 'Nie udało sie zarejestrować...';

				}

			} else {

				echo 'Jest puste';

			}

		}

	}


}

