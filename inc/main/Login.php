<?php
/**
 * loginController Class
 */

namespace endo\main;


class Login extends Validation {

	private $email_user;
	private $password_login;


	/**
	 * Method loginController mainController
	 */
	public function loginForm() {

		if(isset($_POST['action']) && $_POST['action'] == 'user_login') {

			if(!empty($_POST['email']) && !empty($_POST['password'])) {

				$this->email_user = htmlentities($_POST['email'], ENT_QUOTES);

				$this->password_login = htmlentities($_POST['password'], ENT_QUOTES);

				$this->password_login = password_verify($this->password_login, $this->loginPass($this->password_login));

				$this->loginVerify($this->email_user,$this->password_login);

				if($_SESSION['user']) {

					header("Location: main.php");

				} else {

					echo 'Błędny login lub hasło';

				}

			} else {

				echo 'Prosze o podanie danych!';

			}

		}

	}



	/**
	 *
	 * Downloads Users Params
	 *
	 * @return array
	 */
	protected function loginVerify($email,$password) {

		if($this->loginPass($password) || $this->loginEmail($email)) {

			$query = "SELECT * FROM users WHERE email=:email";

			try {

				$result = $this->db->prepare($query);
				$result->bindParam(':email', $email);
				$result->execute();

				$user = $result->fetchAll();

				$_SESSION['user'] = $user;


			} catch (\Exception $e) {

				die($e->getMessage());

			}

		}


	}


	/**
	 *
	 * Downloads Users Params
	 *
	 * @return array
	 */
	protected function loginPass($password) {

		$query = "SELECT pass FROM users WHERE pass=:password LIMIT 1";

		try {

			$result = $this->db->prepare($query);
			$result->bindParam(':password', $password);
			$result->execute();

			$pass = $result->fetch();

			return $pass['pass'] ;


		} catch (\Exception $e) {

			die($e->getMessage());

		}


	}


	protected function loginEmail($email) {

		$query = "SELECT email FROM users WHERE email=:email LIMIT 1";

		try {

			$result = $this->db->prepare($query);
			$result->bindParam(':email', $email);
			$result->execute();

			$email = $result->fetch();

			return $email['email'] ;


		} catch (\Exception $e) {

			die($e->getMessage());

		}


	}



}