<?php
/**
 * Validation
 */
namespace endo\main;


use endo\arrays\ErrorCode;
use endo\db\DB_Connect;

class Validation extends DB_Connect {

	/**
	 *
	 * Validation Email
	 *
	 * @param $email - email
	 * @param $error - Error full name
	 */
	public function emailValidation($email, $error) {

		if(!preg_match("/^[a-zA-Z0-9_.]{2,35}@[a-zA-Z0-9_.ąęćżóńłśĄĘĆŻÓŃŁŚ]{2,35}$/",$email)) {

			echo $error . '<br>';
			unset($email);

		} else {

			return $email;

		}

	}

	/**
	 *
	 *  Validation Text String
	 *
	 * @param $string - text in input
	 * @param $error - Error full name
	 */
	public function stringTextValidation($string, $error) {

		if(!preg_match("/^[a-zA-ZąęćżóńłśĄĘĆŻÓŃŁŚ]{2,35}$/",$string)) {

			return $error;

		} else {

			return $string;

		}

	}


	/**
	 *
	 * Validation Password
	 *
	 * @param $password - password text input
	 * @param $error - Error full name
	 */
	public function validPassword($password, $error) {

		if(!preg_match("/^[a-zA-ZąęćżóńłśĄĘĆŻÓŃŁŚ0-9.-_=+%$#@!]{6,35}$/", $password)) {

				echo $error . '<br>';

		} else {

			return $password;

		}

	}


	/**
	 *
	 * Validation Date
	 *
	 * @param $date - date
	 * @param $error - full error name
	 */
	public function validDate($date, $error) {

		$dateArray = explode('-', $date);

		$day = (int)$dateArray[2];
		$month = (int)$dateArray[1];
		$year = (int)$dateArray[0];

		if($month < 9) {

			$month = 0 . $month;

		}

		/* Verification month, day and year for function checkdate */
		if(checkdate($month , $day , $year)) {

			$date =  $year . '-' . $month . '-' . $day;
			return $date;

		} else {

			echo $error;

		}

	}


	/**
	 *
	 * Veryfication passwords
	 *
	 * @param $pass1 - password verify 1
	 * @param $pass2 - password verify 2
	 * @param $error - full name error
	 */
	public function verificationPasswords($pass1, $pass2, $error) {

		if($pass1 !== $pass2) {

			echo $error;

		} else {

			return TRUE;

		}


	}


	/**
	 *
	 * Verify Email repeated
	 *
	 * @param $db_name
	 * @param $email
	 * @param $val
	 *
	 * @return bool
	 */
	public function emailVerifyDb($db_name, $email, $val) {

		$query = "SELECT * FROM `$db_name`";

		$result = $this->db->query($query);
		$result->execute();
		$key = $result->fetchAll();

		foreach ($key as $value) {

			$emvery= $value[$val];

		}

		if($email !== $emvery) {

			return $email;

		} else {

			$email = FALSE;
			echo 'Podany adres email istnieje w bazie';

		}

	}


	/**
	 *
	 * Weryfikuje dwa parametry czy sa takie same
	 *
	 * @param $key1
	 * @param $key2
	 *
	 * @return bool
	 */
	public function searchRepeatedVal($key1,$key2) {

		if($key1 != $key2) {

			$key = false;
			return $key;

		} else {

			$key = true;
			return $key;

		}

	}


	/**
	 * Verify price and return price ok format
	 *
	 * @param $price
	 * @param $error
	 *
	 * @return string
	 */
	public function priceNumber($price, $error) {

		if (is_numeric($price)) {

			$price = number_format($price,2,'.' , ' ');

			return $price;

		} else {

			return $error;

		}

	}


}