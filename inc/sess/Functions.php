<?php

namespace endo\sess;


use endo\arrays\ErrorCode;
use endo\main\Validation;
use endo\sess\User;

class Functions extends Validation {


	/**
	 * Saved Tank Method
	 */
	public function addTank() {

		if(isset($_POST['send_price_tank']) && isset($_POST['token_1']) )  {



			if(!empty($_POST['title_tank']) && !empty($_POST['date_tank']) && !empty($_POST['price_tank']) && !empty($_POST['count_tank'])) {

				$title_tank = $_POST['title_tank'];
				$price_tank = $_POST['price_tank'];
				$count_tank = $_POST['count_tank'];
				$date_tank = $_POST['date_tank'];

				/* Validation Title */
				$title_tank = $this->stringTextValidation($title_tank, ErrorCode::registerErrorCode('verifyTitle'));
				$title_tank = htmlentities($title_tank, ENT_QUOTES);

				/* Validation Litres */
				$count_tank = $this->priceNumber($count_tank, ErrorCode::registerErrorCode('verifyLitres'));
				$count_tank = htmlentities($count_tank, ENT_QUOTES);

				/* Validation Price */
				$price_tank = $this->priceNumber($price_tank, ErrorCode::registerErrorCode('verifyPrice'));
				$price_tank = htmlentities($price_tank, ENT_QUOTES);


				/* Validation Date */
				$date_tank = $this->validDate($date_tank, ErrorCode::registerErrorCode('date'));
				$date_tank = htmlentities($date_tank, ENT_QUOTES);

				$category = 'tank';

				$userId = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : false;


				if($_SESSION['token1'] === $_POST['token_1']) {

					try {

						$query = "INSERT INTO user_events(userId, category_event,title_event,count_event,price_event,date_event) VALUES(:userId,:category,:title,:counte,:price,:datee)";

						$result = $this->db->prepare( $query );
						$result->bindParam( ":userId", $userId );
						$result->bindParam( ":category", $category );
						$result->bindParam( ":title", $title_tank );
						$result->bindParam( ":counte", $count_tank );
						$result->bindParam( ":price", $price_tank );
						$result->bindParam( ":datee", $date_tank );
						$result->execute();


						echo 'Tankowanie zostało zapisane.';


					} catch ( \Exception $e ) {

						echo $e->getMessage();

					}

				}

			} else {

				echo 'Pola nie mogą byc puste.';

			}

		}


	}


	/**
	 *  Select all information tank where userId
	 */
	public function selectEvents($category) {

		$userId = $_SESSION['user_id'];

		$date = $_SESSION['month'] . '-01';
		$date_end = $_SESSION['month'] . '-31';

		$query = "SELECT * FROM user_events WHERE userId=$userId AND category_event='$category' AND date_event >= '$date' AND date_event <= '$date_end' ORDER BY date_event ASC";
		//$query = "SELECT * FROM user_events WHERE userId=$userId AND category_event='$category' AND date_event > $date ORDER BY date_event ASC";

		$vals = $this->queryAllDb($query);

		return $vals;

	}


	public function sessionEvents() {

		$query = "SELECT * FROM user_events";
		$vals = $this->queryAllDb($query);

		foreach ($vals as $event) {

			$_SESSION['eventId'] = $event['eventId'];
			$_SESSION['userId'] = $event['userId'];
			$_SESSION['category_event'] = $event['category_event'];
			$_SESSION['title_event'] = $event['title_event'];
			$_SESSION['count_event'] = $event['count_event'];
			$_SESSION['price_event'] = $event['price_event'];
			$_SESSION['date_event'] = $event['date_event'];

		}

	}



	/**
	 *  Add Bills method
	 */
	public function addBills() {

		if(isset($_POST['send_price_bills'])) {

			if(!empty($_POST['title_bills']) && !empty($_POST['price_bills']) && !empty($_POST['date_bills'])) {

				$title_bills = $_POST['title_bills'];
				$price_bills = $_POST['price_bills'];
				$date_bills = $_POST['date_bills'];

				/* Validation Title Bills */
				$title_bills = $this->stringTextValidation($title_bills, ErrorCode::registerErrorCode('verifyTitle'));
				$title_bills = htmlentities($title_bills, ENT_QUOTES);

				/* Validation Price Bills */
				$price_bills = $this->priceNumber($price_bills, ErrorCode::registerErrorCode('verifyPrice'));
				$price_bills = htmlentities($price_bills, ENT_QUOTES);

				/* Validation Date Bills */
				$date_bills = $this->validDate($date_bills, ErrorCode::registerErrorCode('date'));
				$date_bills = htmlentities($date_bills, ENT_QUOTES);

				$category = 'bills';
				$count = '  ';

				if($_POST['token_2'] === $_SESSION['token2']) {

					try {

						$query = $query = "INSERT INTO user_events(userId, category_event,title_event,count_event,price_event,date_event) VALUES(:userId,:category,:title,:counte,:price,:datee)";

						$result = $this->db->prepare($query);
						$result->bindParam(":userId", $_SESSION['user_id']);
						$result->bindParam(":category", $category);
						$result->bindParam(":title", $title_bills);
						$result->bindParam(":counte", $count);
						$result->bindParam(":price", $price_bills);
						$result->bindParam(":datee", $date_bills);
						$result->execute();


						echo 'Rachunek został zapisany.';


					} catch ( \Exception $e ) {

					echo $e->getMessage();

					}

				}

			} else {

				echo 'Prosze o wypełnienie wszystkich pól...';

			}

		}

	}


}