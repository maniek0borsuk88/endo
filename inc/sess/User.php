<?php
/**
 * Users Settings Configuration Class
 */

namespace endo\sess;


use endo\main\Login;

class User extends Login {

    private $user_id;
    public $user_name;
    public $user_surname;
    public $user_email;
    private $user_pass;
    private $user_passRepeat;
    public $user_country;
    public $user_dateBirth;
    public $user_dateRegister;
    public $user_imgProf;


	/**
     *
	 * Download User active
     *
	 */
	public function profilUser() {

	    if(isset($_SESSION['user'])) {

		    foreach ( $_SESSION['user'] as $user ) {

                $this->user_id = $user['user_id'];
                $_SESSION['user_id'] = $this->user_id;

                $this->user_name = $user['imie'];
                $_SESSION['user_name'] = $this->user_name;

                $this->user_surname = $user['nazwisko'];
                $this->user_email = $user['email'];
                $this->user_pass = $user['pass'];
                $this->user_passRepeat = $user['pass_repeat'];
                $this->user_country = $user['kraj'];
                $this->user_birthDate = $user['date_birth'];
                $this->user_dateRegister = $user['date_register'];
                $this->user_imgProf = $user['img_profil'];

	        }

        }

    }


	public function generateProfilImg() {

		if(isset($_POST['change_img'])) {

			if(!empty($_FILES['obrazek'])) {


				//$path = SERVER_PATH_LINK . '/../../public/img/' . $_FILES['obrazek']['name'];
				$path = 'img/' . $_FILES['obrazek']['name'];
				$query = "UPDATE `users` SET `img_profil`=:path WHERE `user_id`=$this->user_id";
				$result = $this->db->prepare($query);
				$result->bindParam(":path", $path);
				$result->execute();

				$this->select();

			}

		}

	}


	/**
     *
     * Upload file in Server - Modal
     *
	 * @return bool
	 */
	public function uploadFile() {


	    if(isset($_POST['change_img'])) {

	        $file = $_FILES['obrazek'];

	        print_r($file); // Wyświetla zawartosc zmiennej Files - usuń to pózniej

	        $fileName = $_FILES['obrazek']['name'];
	        $fileTmpName = $_FILES['obrazek']['tmp_name'];
	        $fileSize = $_FILES['obrazek']['size'];
	        $fileError = $_FILES['obrazek']['error'];
		    $fileType = $_FILES['obrazek']['type'];

		    $fileExt = explode('.', $fileName);
		    $fileActualExt = strtolower(end($fileExt));

		    $allowed = ['jpg','jpeg', 'png','pdf'];

		    if(in_array($fileActualExt, $allowed)) {

                if($fileError === 0) {

                    if($fileSize < 1000000) {

	                    $fileDestination = $fileDestination =  PATH_PUBLIC . '/../img/' . $fileName;
	                    if(is_uploaded_file($fileTmpName))
	                    {
		                    if(!move_uploaded_file($fileTmpName, $fileDestination))
		                    {
			                    echo 'problem: Nie udało się skopiować pliku do katalogu.';
			                    return false;
		                    }
	                    }
	                    else
	                    {
		                    echo 'problem: Możliwy atak podczas przesyłania pliku.';
		                    echo 'Plik nie został zapisany.';
		                    return false;
	                    }

	                    return true;

                    } else {

                        echo 'Przesyłany plik jest za duży.';

                    }

                } else {

                    echo 'Nie udało się załadowac pliku.';

                }

            } else {

		        echo 'Zły format przesyłanych plików.';

            }

        }

    }


    public function select() {

		$query = "SELECT * FROM users WHERE user_id=:id";

		$result = $this->db->prepare($query);
		$result->bindParam(":id", $this->user_id);
		$result->execute();
		$_SESSION['user'] = $result->fetchAll();
		header("Refresh:0");

    }




	public function getId() {

		$this->profilUser();

		echo $this->user_id;

	}

	/**
	 * @return mixed
	 */
	public function getUserId() {
		return $this->user_id;
	}


}