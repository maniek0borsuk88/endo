<?php include  'sidebar.php';

?>

<div id="profil-container">

    <div class="container-fluid">
        <div class="row">
            <div class="col-6">
                <h1 class="text-center"><?php echo $cal->html_title;?></h1>
                <?php echo $_SESSION['month']; ?>
                <a href="calendar.php?date=<?php echo $cal->prev;?>">&lt;</a><h1 class="text-center"></h1><a href="calendar.php?date=<?php echo $cal->next;?>">&gt;</a>
                <table class="calendar table table-bordered">
                    <tr>
                        <th class="week-day">Pn</th>
                        <th class="week-day">Wt</th>
                        <th class="week-day">Śr</th>
                        <th class="week-day">Cz</th>
                        <th class="week-day">Pt</th>
                        <th class="week-day">Sb</th>
                        <th class="text-danger week-day">Nd</th>
                    </tr>

                    <?php
                    foreach ($cal->weeks as $week) {
                        echo  $week ;
                    }
                    ?>

                </table>

            </div>



            <div class="col-6">

                <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-footer">
                                <form enctype="multipart/form-data" method="post" >
                                    <input type="hidden" name="MAX_FILE_SIZE" value="512000000" />
                                    <input type="file" name="obrazek" />
                                    <input type="submit" value="wyślij" name="change_img"/>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="nav-user-page m-2">
                    <a href="main.php" class="btn btn-primary">Powrót do Strony głównej</a>
                </div>

                <?php include 'modals/add_statoil.php'; ?>

                <?php include 'modals/add_cost.php'; ?>



                <button type="button" class="btn btn-primary m-2" data-toggle="modal" data-target="#rachunki" data-whatever="@getbootstrap">Dodaj Wydarzenie: </button>



            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Data</th>
                        <th scope="col">Title</th>
                        <th scope="col">Ilość</th>
                        <th scope="col">Cena</th>
                        <th scope="col">Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(isset($_GET['delete'])) {

	                    $del= ($_GET['delete']);
	                    $delete->delete("DELETE FROM user_events WHERE eventId=$del LIMIT 1");

                    }



                    $i = 1;
                    foreach ($tanks->selectEvents('tank') as $val) :?>
                        <tr>
                            <th scope="row"><?php echo $i++; ?></th>
                            <td><?php echo $val['date_event']; ?></td>
                            <td><?php echo $val['title_event']; ?></td>
                            <td><?php //echo $val['count_event']; ?> <small></small></td>
                            <td><?php echo $val['price_event']; ?> <small>zł</small></td>
                            <td><a href="calendar.php?delete=<?php echo $val['eventId']; ?>" name="delete">delete</a></td>
                        </tr>
	                    <?php include 'modals/delete.php'; ?>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Suma</th>
                        <td></td>
                        <td></td>
                        <td>
                            <strong>
                                <?php /*$userId = $_SESSION['user_id']; echo $delete->sumTank("SELECT SUM(count_event) FROM user_events WHERE userId=$userId AND category_event='tank'", "SUM(count_event)"); */?><!-- Litrów-->
                            </strong>
                        </td>

                        <td class="padding-sum">
                            <strong>
                                <?php
                                $date = $_SESSION['month'] . '-01';
                                $date_end = $_SESSION['month'] . '-31';
                                ?>
                                <?php $userId = $_SESSION['user_id']; echo $delete->sumTank("SELECT SUM(price_event) FROM user_events WHERE userId=$userId AND category_event='tank' AND date_event >= '$date' AND date_event <= '$date_end'","SUM(price_event)"); ?> zł
                            </strong>
                        </td>
                        <td></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="col-6">


            </div>
        </div>
    </div>

</div>
<?php include 'footer.php'; ?>



