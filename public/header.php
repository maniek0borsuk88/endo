<?php


require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'config.php';

if(isset($_SESSION['user'])) {

    header("Location: main.php");

}

?>
<!doctype html>
<html lang="pl">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Endo Kalendarz Treningowy</title>
	<link rel="stylesheet" href="<?php echo 'style.css'; ?>" type="text/css" media="all">
</head>

<body>
