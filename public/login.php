<?php include 'header.php'; ?>


    <div id="main-login">
        <div class="container-fluid">
            <div class="img-welcome">
                <img src="<?php echo 'img/loginBg.jpg'; ?>" alt="">
            </div>
            <div id="form-login" class="container center-register">
                <h2 class="text-center">Zaloguj się....</h2>
                <div class="row">
                    <div class="col-12 col-sm-1 col-md-3"></div>
                    <div class="col-12 col-sm-10 col-md-6">
                        <form method="post" class="form-login">

                            <?php $login = new \endo\main\Login();
                            echo $login->loginForm();
                            ?>

                            <!-- Email -->
                            <div class="form-group">
                                <label>Email:</label>
                                <input type="email" name="email" class="form-control" placeholder="Email">
                            </div>

                            <!-- Password -->
                            <div class="form-group">
                                <label>Hasło: </label>
                                <input type="password" name="password" class="form-control"  placeholder="Hasło">
                            </div>
                            <input type="hidden" name="token" value="<?php $_SESSION['token']; ?>"/>
                            <input type="hidden" name="action" value="user_login"/>
                            <input type="submit" name="submit" value="Zaloguj się" class="btn btn-primary"/>
                            <p>Nie masz jeszcze konta? <a href="register.php">Zarejestruj się</a> i planuj swój miesiąc treningowy z wyprzedzeniem już teraz!</p>
                        </form>
                    </div>
                    <div class="col-12 col-sm-1 col-md-3"></div>
                </div>
            </div>
        </div>
    </div>
</header>

<?php include 'footer.php'; ?>