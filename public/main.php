<?php include 'sidebar.php'; ?>

<div id="profil-container">
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="jumbotron">
                <h1 class="display-4">Witaj <?php echo $_SESSION['user_name']; ?>!</h1>
                <h3 class="lead">Witaj w Głównym Panelu Użytkownika</h3>
                <hr class="my-4">
            </div>
        </div>
        <div class="col-12 col-sm-4">
            <!-- Card Calendar -->
            <div class="card">
                <img class="card-img-top" src="..." alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Kalendarz</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href="calendar.php?date=<?php echo date('Y-m'); ?>" class="btn btn-primary">Przejdz do Kalendarza</a>
                </div>
            </div>
        </div>
    </div>
        <div class="row">

        </div>

    </div>
</div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-footer">
                    <form enctype="multipart/form-data" method="post" >
                        <input type="hidden" name="MAX_FILE_SIZE" value="512000000" />
                        <input type="file" name="obrazek" />
                        <input type="submit" value="wyślij" name="change_img"/>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

<?php include 'footer.php'; ?>



