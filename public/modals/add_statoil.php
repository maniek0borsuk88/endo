<div class="add_event">
	<?php $tanks->addTank(); ?>
	<button type="button" class="btn btn-primary m-2" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Dodaj Tankowanie: </button>

	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Dodaj Tankowanie: </h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<form method="post" action="calendar.php">
						<div class="form-group">
							<label class="col-form-label">Tytuł: </label>
							<input type="text" class="form-control" name="title_tank">
						</div>
						<div class="form-group">
							<label class="col-form-label">Cena: </label>
							<input type="text" class="form-control" name="price_tank">
						</div>
						<div class="form-group">
							<label class="col-form-label">Ilość (litrów) : </label>
							<input type="text" class="form-control" name="count_tank">
						</div>
						<div class="form-group">
							<label class="col-form-label">Wybierz datę: </label>
							<input type="date" class="form-control" name="date_tank">
						</div>
						<div class="modal-footer">
                            <input type="hidden" name="token_1" value="<?php echo $_SESSION['token1'] = md5(time()); ?>">
							<input type="submit" class="btn btn-primary" name="send_price_tank" placeholder="Zapisz">
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
</div>