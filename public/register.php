<?php include 'header.php'; ?>

<header class="header-welcome">
    <div class="container-fluid navbar-welcome">

        <!-- Navigation -->
        <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="#">
                Bootstrap
            </a>
            <ul class="nav justify-content-end">
                <p>Masz już konto?</p>
                <a class="nav-link btn btn-primary" href="login.php ">Zaloguj się...</a>
            </ul>
        </nav>
    </div>


	<div class="main-login">
		<div class="container-fluid">

			<!-- Image Background -->
			<div class="img-welcome">
				<img src="<?php echo 'img/background.jpg'; ?>" alt="">
			</div>
			<div class="main-content">
				<div class="container center-register">
					<div class="row">
						<div class="col-md-12 col-lg-6">
							<div class="welcome-text">
								<h3>Witaj na stronie Kalendarza Biegowego...</h3>
								<p>Jeśli chcesz terminowo planować swój miesięczny kilometraż z wyprzedzeniem i zapisywaniem swojej historii... </p>
								<p>Zapraszam do zarejstrowania się do Kalendarza Biegowego.... </p>
							</div>
						</div>

						<div class="col-12 col-lg-6">

							<form class="form-register" method="post">
								<?php  use endo\main\FormRegister;
								$formRegister = new FormRegister();
								$formRegister->formRegister();
								?>
								<!-- Name -->
								<div class="form-group">
									<label>Imię:</label>
									<input type="text" name="name" class="form-control" placeholder="Imię">
								</div>

								<!-- Surname -->
								<div class="form-group">
									<label>Nazwisko:</label>
									<input type="text" name="surname" class="form-control" placeholder="Nazwisko">
								</div>

								<!-- Email -->
								<div class="form-group">
									<label>Email:</label>
									<input type="email" name="email" class="form-control" placeholder="Email">
								</div>

								<!-- Password -->
								<div class="form-group">
									<label>Hasło: </label>
									<input type="password" name="password" class="form-control"  placeholder="Hasło">
								</div>

								<!-- Password Repeat -->
								<div class="form-group">
									<label>Powtórz Hasło: </label>
									<input type="password" name="repeat_password" class="form-control"  placeholder="Powtórz Hasło">
								</div>

								<div class="form-row">

									<!-- Country -->
									<div class="form-group col-md-6">
										<label>Kraj: </label>
										<input type="text" name="country" class="form-control"  placeholder="Kraj">
									</div>

									<!-- Date of Birth -->
									<div class="form-group col-md-6">
										<div class="form-group">
											<label for="inputCity">Data urodzenia:</label>
											<input type="date" name="day_birth" class="form-control">
										</div>
									</div>
								</div>

								<!-- Submit form Register -->
								<button type="submit" name="register" class="btn btn-primary">Zarejestruj się</button>
							</form>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</header>

<?php include 'footer.php'; ?>